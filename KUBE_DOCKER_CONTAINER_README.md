# API-exercise

Name: Brunda A B
email: brunda27anaji@gmail.com

This exercise was indeed a brain exercise for me because it has new things of software development of API which I got an opportunity to learn in these last 10days to try out as much i could. However I had hands on for the rest part of running an application as a dockerized build & clustering service through kubernetes. I thanks Container Solution for giving me this exercise which inturn helped me learn something out of my comfort areas.  

## Exercise
Below are the explanation of implementation done by me for the corresponding tasks.

### 0. I have Forked this repository
The link is sent over the email.

### 1. Setup & fill database
Using Mongdb
Creating: titanic_db & exporting the data from csv-file using mongoimport option in Mongodb

### 2. Create an API
Created an API project, which runs using node.js, connects to mongodb & implemented the HTTP end points (REST). 
However I found it challenging while getting the HTTP end points to work as per the requirement because of my limited experience on this.

### 3. Dockerize
The app is dockerized. I have used docker-compose to build. 
Dockerfile - runs & creates api node image
docker-compose.yml - creates services of app container &  connects to mongodb container which imports csv file data

### 4. Deploy to Kubernetes
Docker containers are deployed on a Kubernetes cluster.
$kubectl run <mycluster_name> --image=<api_app_image_name> --port=8080
$kubectl expose deployment <mycluster_name> --type="LoadBalancer" 
$kubectl get services <mycluseter_name>

with the EXTERNAL_IP value from above cmd run on browser "EXTERNAL_IP:8080" gives my clusterized containerzed HTTP API services


