//server.js
const express = require('express');
const app = express();
const mongodb = require('mongodb');

const config = require('./database.js');
const PORT = 4000;
const client = mongodb.MongoClient;

client.connect(config.DB, function(err, db) {
    if(err) {
        console.log('titanic database is not connected')
    }
    else {
        console.log('Titanic Database is Connected!!')
    }
});

app.get('/', function(req, res) {
    res.json({"hello": "world"});
});

app.listen(PORT, function(){
    console.log('Your node js server is running on PORT:',PORT);
});

