let PassengerModel = require('../models/Passenger.model')
let express = require('express')
let router = express.Router()


// POST localhost:4000/Passenger
router.post('/psg_list', (req, res) => {
    if(!req.people) {
        return res.status(400).send('Request person is missing')
    }

    if(!req.people.Name) {
            }

        let model = new PassengerModel.req.body)
    model.save()
        .then(doc => {
            if(!doc || doc.length === 0) {
                return res.status(500).send(doc)
            }

            res.status(201).send(doc)
        })
        .catch(err => {
            res.status(500).json(err)
        })
})

// GET
router.get('/people', (req, res) => {
    if(!req.query.people) {
        return res.status(400).send('Missing URL parameter: passenger')
    }

    PassengerModel.findOne({
        email: req.query.Name
    })
        .then(doc => {
            res.json(doc)
        })
        .catch(err => {
            res.status(500).json(err)
        })
})


// DELETE
router.delete('/passenger', (req, res) => {
    if(!req.query.name) {
        return res.status(400).send('Missing URL parameter: name')
    }

    PassengerModel.findOneAndRemove({
        email: req.query.name
    })
        .then(doc => {
            res.json(doc)
        })
        .catch(err => {
            res.status(500).json(err)
        })
})

// PUT
router.put('/passenger', (req, res) => {
    if(!req.query.name) {
        return res.status(400).send('Missing URL parameter: name')
    }

    PassengerModel.findOneAndUpdate({
        name: req.query.name
    }, req.body, {
        new: true
    })
        .then(doc => {
            res.json(doc)
        })
        .catch(err => {
            res.status(500).json(err)
        })
})
module.exports = router
